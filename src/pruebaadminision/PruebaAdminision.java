/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebaadminision;


import clases.Punto;
import clases.Rect;
import java.awt.Point;
import java.awt.Rectangle;



/**
 *
 * @author Latinon
 */
public class PruebaAdminision {
 public  int x ,y,width,height;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
 
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
    PruebaAdminision cuerpo = new PruebaAdminision();
    //USANDO LA CLASE RECTANGLE DE AWT    
    //Por qué no usar JDK API para hacer esto y nos ahorramos la creacion de la clase.
    Rectangle rect1 = new Rectangle(100, 100, 200, 240); 
    Rectangle rect2 = new Rectangle(120, 80, 80, 120); 
    Rectangle rect3 = new Rectangle(140, 60, 50, 70); 
    Punto punto1 = new Punto(20,40);
    Punto punto2 = new Punto(30,20);
    Punto punto3 = new Punto(20,40);
    Punto punto4 = new Punto(30,20);
    Rect rectangulo1 = new Rect(punto1, punto2);
    Rect rectangulo2 = new Rect(punto2, punto3);
    Rect rectangulo3 = new Rect(punto3, punto1);
  
    /*
    Para usar la java.awt.Rectangleclase, los parámetros del constructor son: x, y, 
    ancho, altura, en la que x, y son la esquina superior izquierda del rectángulo.
    Puede convertir fácilmente el punto inferior izquierdo a superior izquierdo.
    */
    //Rectangle intersection = rect1.intersection(rect2);
  
    cuerpo.intersectionEntreRectangulos(rect1,rect2,rect3);
    cuerpo.setX(20);
    cuerpo.setY(20);
    cuerpo.setHeight(40);
    cuerpo.setWidth(40);
    cuerpo.intersection(rect1);
   
}
     private static boolean intersectionEntreRectangulos(Rectangle r1, Rectangle r2,  Rectangle out) { 
        float xmin = Math.max(r1.x, r2.x); 
        float xmax1 = r1.x + r1.width; 
        float xmax2 = r2.x + r2.width; 
        float xmax = Math.min(xmax1, xmax2); 
        if (xmax > xmin) { 
        float ymin = Math.max(r1.y, r2.y); 
        float ymax1 = r1.y + r1.height; 
        float ymax2 = r2.y + r2.height; 
        float ymax = Math.min(ymax1, ymax2); 
            if (ymax > ymin) { 
             out.x = (int) xmin; 
             out.y = (int) ymin; 
             out.width = (int) (xmax - xmin); 
             out.height = (int) (ymax - ymin); 
            System.out.println("ES TRUE POR QUE SE INTERSECTAN LOS RECTANGULOS");
             return true; 
            }
        } 
    return false; 
} 
     
        public Rectangle intersection(Rectangle r) { 
        int tx1 = this.x; 
        int ty1 = this.y; 
        int rx1 = r.x; 
        int ry1 = r.y; 
        long tx2 = tx1; tx2 += this.width; 
        long ty2 = ty1; ty2 += this.height; 
        long rx2 = rx1; rx2 += r.width; 
        long ry2 = ry1; ry2 += r.height; 
        if (tx1 < rx1) tx1 = rx1; 
        if (ty1 < ry1) ty1 = ry1; 
        if (tx2 > rx2) tx2 = rx2; 
        if (ty2 > ry2) ty2 = ry2; 
        tx2 -= tx1; 
        ty2 -= ty1; 
       
        if (tx2 < Integer.MIN_VALUE) tx2 = Integer.MIN_VALUE;

        if (ty2 < Integer.MIN_VALUE) ty2 = Integer.MIN_VALUE; 

        return new Rectangle(tx1, ty1, (int) tx2, (int) ty2); 
    } 
    
}
