package clases;


public class Rect {
    public Punto ul;
    public Punto lr;
    public int  width; 
    public int height; 
    public int x; // the center in x axis 
    public int y; // the center in y axis 
    

    public Rect(Punto ul, Punto lr) {
        this.ul = ul;
        this.lr = lr;
    }

   
    
    boolean intersects(Rect A, Rect B){
        boolean sw=false;
        
        if (Math.abs(A.x - B.x) < (Math.abs(A.width + B.width)/2) 
        && (Math.abs(A.y - B.y) < (Math.abs(A.height + B.height)/2))){
            sw=true;
        } else{
            sw=false;
        }
        return sw;
    }
    
}
