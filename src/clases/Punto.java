package clases;


public class Punto {
    public int x;
    public int y;

    public Punto(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    public Punto() {
    this(0,0);
  }
  
 
  
  public Punto(Punto p) {
    x = p.x; //p.getX();
    y = p.y; //p.getY();
  }
  

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
    
    public void desplazar(int deltaX, int deltaY) {
    x += deltaX; // x = x + deltaX;
    y += deltaY;
  }
  
  public double distancia(Punto p) {
    //Math.pow(x-p.x,2) + Math.pow(y-p.y,2)
    return Math.sqrt((double)((x-p.x)*(x-p.x) +
                              (y-p.y)*(y-p.y)));
  }
  
  public boolean estanAlineados(Punto p1, Punto p2) {
    return (y - p1.y)*(p2.x - p1.x) == (p2.y - p1.y)*(x -p1.x);
  }
  
  public boolean equals(Punto p) {
    return x == p.x && y == p.y;
  }
  
  public String toString() {
    return "(" + x + "," + y + ")";
  }
}
